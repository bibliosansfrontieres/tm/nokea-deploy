# nokea

A script to Deploy an Approve'd project onto a CAP.

## Usage

First, connect to the device (the factory password is `123lkj`):

```shell
ssh cap@10.11.12.13
```

Become `root`:

```shell
sudo -i
```

Download and run the script all at once:

```shell
wget https://gitlab.com/bibliosansfrontieres/tm/nokea-deploy/-/raw/master/go.sh --quiet -O- | bash -
```

Please note that you can override OLIP catalog by launching `deploy.py` right after the command above

```shell
export olip_descriptor="https://s3.eu-central-1.wasabisys.com/olip-catalog-descriptor/prod/amd64/"
/usr/local/bin/deploy.py
```

What did just happen? This script installed a cronjob that will check
whether there is an Approve'd project linked to the device's MAC address.
As soon as it will find one, it will proceed to installation.

## Monitor

How do we know the installation process is running correctly?

### See the system logs

The installation will install a bunch of packages. This may be the very first
sign of things moving, since it has to install `ansible` at the very beginning.
The OS package manager will write logs to  `/var/log/apt/history.log`.

The playbooks also write their output to `/var/log/ansible-pull.log`.

Because these logs show quickly and efficently the installaiton process,
the script automatically displays these logs.
You can stop the log stream by using the key combination `Ctrl C`.

To show these logs again:

```shell
tail -F /var/log/ansible-pull.log /var/log/apt/history.log
```

### See the lock files

As the installation happen, lock files will be written at
`/opt/olip-pre-install`, `/opt/olip-post-install`,
`/opt/olip-deploy`, `/opt/rebooted`.
This will avoid the script to run multiple time the same installation.

This command will show you these lock files, timestamp included:

```shell
ls -l /opt
```

Example output:

```text
# ls -l /opt/
total 4
drwx--x--x 4 root root 4096 Jul  1 15:52 containerd
-rw-r--r-- 1 root root    0 Aug  9 15:20 olip-deploy
-rw-r--r-- 1 root root    0 Aug  9 15:21 olip-post-install
-rw-r--r-- 1 root root    0 Aug  9 15:15 olip-pre-install
-rw-r--r-- 1 root root    0 Aug  9 15:21 rebooted
```

Here, it seems the very first OLIP install was done on July, 1st;
but the latest Project Deploy happened on August, 9th.

### See olip-api logs

At some point, the wole OLIP stack is installed and the device is restarted.

From now on, not much will be relevant in the previously mentionned logfiles.
We now need to check whether OLIP API is doing its job.

Reconnect tto the device, become `root` again, then check the `api` container logs:

```shell
balena-engine logs -tf api
```

What we want to see, is applications and contents in the installation queue.

First, it will install applications. Here, the mediacenter installation is starting:

```text
2021-03-24T12:54:03.322420191Z 2021-03-24 12:54:03,321 - StateMachine - DEBUG - Found 1 applications to handle
2021-03-24T12:54:03.324399729Z 2021-03-24 12:54:03,323 - StateMachine - DEBUG - Launching task for application mediacenter.app from state ApplicationState.uninstalled to state ApplicationState.installed
2021-03-24T12:54:03.327103157Z 2021-03-24 12:54:03,326 - StateMachine - DEBUG - Current task <DownloadTask(Thread-4, started 3014425408)> running. Don't do anything for now
2021-03-24T12:54:03.341451266Z 2021-03-24 12:54:03,340 - DownloadTask - INFO - Pulling image offlineinternet/hugo-mediacenter-i386:latest
```

Once all applications are installed, contents will follow. Here, it finds that there are 224 contents remaining:

```shell
2021-03-24T11:03:37.272169876Z 2021-03-24 11:03:37,271 - StateMachine - DEBUG - Ping of state machine id <app.applications.state_machine.state_machine.StateMachine object at 0xb46a58e0>, current task is None
2021-03-24T11:03:37.292421484Z 2021-03-24 11:03:37,291 - StateMachine - DEBUG - Found 0 applications to handle
2021-03-24T11:03:37.711839590Z 2021-03-24 11:03:37,711 - StateMachine - DEBUG - Found 224 contents to handle
```

Obviously, we want to see this number to decrease. You can see content packages being downloaded and installed:

```text
2021-03-24T12:58:03.616039562Z 2021-03-24 12:58:03,613 - StateMachine - DEBUG - Ping of state machine id <app.applications.state_machine.state_machine.StateMachine object at 0xb46548b0>, current task is <InstallContentTask(Thread-6, stopped 3014425408)>
2021-03-24T12:58:03.618140300Z 2021-03-24 12:58:03,616 - StateMachine - DEBUG - Task <InstallContentTask(Thread-6, stopped 3014425408)> seems to be dead. Removing lock
2021-03-24T12:58:03.627009208Z 2021-03-24 12:58:03,626 - StateMachine - DEBUG - Found 0 applications to handle
2021-03-24T12:58:03.635908849Z 2021-03-24 12:58:03,635 - StateMachine - DEBUG - Found 13 contents to handle
2021-03-24T12:58:03.636828735Z 2021-03-24 12:58:03,635 - StateMachine - DEBUG - Launching task for content fle-niveau-3-a1-fra to state ContentState.installed
2021-03-24T12:58:03.746551883Z 2021-03-24 12:58:03,745 - urllib3.connectionpool - DEBUG - Starting new HTTP connection (1): s3.eu-central-1.wasabisys.com:80
2021-03-24T12:58:03.973329062Z 2021-03-24 12:58:03,972 - urllib3.connectionpool - DEBUG - http://s3.eu-central-1.wasabisys.com:80 "GET /olip-packages-prod/hugo/fle-niveau-3-a1-fra.zip HTTP/1.1" 200 37889152
2021-03-24T12:59:03.686465916Z no file that prove content has been added created
2021-03-24T12:59:03.686622266Z 2021-03-24 12:59:03,685 - schedule - INFO - Running job Every 1 minute do ping_state_machine() (last run: 2021-03-24 12:58:03, next run: 2021-03-24 12:59:03)
2021-03-24T12:59:03.686882273Z 2021-03-24 12:59:03,686 - StateMachine - DEBUG - Ping of state machine id <app.applications.state_machine.state_machine.StateMachine object at 0xb46548b0>, current task is <InstallContentTask(Thread-7, stopped 3014425408)>
2021-03-24T12:59:03.686958542Z 2021-03-24 12:59:03,686 - StateMachine - DEBUG - Task <InstallContentTask(Thread-7, stopped 3014425408)> seems to be dead. Removing lock
2021-03-24T12:59:03.715965660Z 2021-03-24 12:59:03,699 - StateMachine - DEBUG - Found 0 applications to handle
2021-03-24T12:59:03.716143264Z 2021-03-24 12:59:03,711 - StateMachine - DEBUG - Found 12 contents to handle
2021-03-24T12:59:03.716418807Z 2021-03-24 12:59:03,712 - StateMachine - DEBUG - Launching task for content fle-niveaux-3-et-4-fos-emploi-fra to state ContentState.installed
2021-03-24T12:59:03.826133138Z 2021-03-24 12:59:03,825 - urllib3.connectionpool - DEBUG - Starting new HTTP connection (1): s3.eu-central-1.wasabisys.com:80
2021-03-24T12:59:04.131423271Z 2021-03-24 12:59:04,130 - urllib3.connectionpool - DEBUG - http://s3.eu-central-1.wasabisys.com:80 "GET /olip-packages-prod/hugo/fle-niveaux-3-et-4-fos-emploi-fra.zip HTTP/1.1" 200 9745998
```

The last thing the installation will do,
is writing a lock file at `/opt/is_installed`.
