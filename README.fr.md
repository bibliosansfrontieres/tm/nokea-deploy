# nokea

Un script pour Deploy un projet Apprové sur un CAP.

## Utilisation

Tout d'abord, il faut se connecter au CAP (le mot de passe d'usine est `123lkj`) :

```shell
ssh cap@10.11.12.13
```

Devenir `root` :

```shell
sudo -i
```

Télécharger et lancer le script d'un seul coup :

```shell
wget https://gitlab.com/bibliosansfrontieres/tm/nokea-deploy/-/raw/master/go.sh --quiet -O- | bash -
```

A noter qu'il est possible de surcharger le catalogue d'OLIP en forçant `deploy.py` immédiatement après la commande ci-dessus

```shell
export olip_descriptor="https://s3.eu-central-1.wasabisys.com/olip-catalog-descriptor/prod/amd64/"
/usr/local/bin/deploy.py
```

Que vient-il de se passer ? Le script a installé un cronjob qui vérifie
s'il existe un projet Approve lié à l'adresse MAC du CAP.
Aussitôt qu'il en trouve un, il procède à l'installation.

## Surveillance

Comment savoir que l'opération se déroule correctement ?

### Vérifier les logs système

L'installation va ajouter une poignée de paquets logiciels au système.
C'est là le tout premier signe que les choses bougent,
car on a besoin du paquet `ansible` dès le début.
Le gestionnaire de paquets `apt` va écrire ses logs dans `/var/log/apt/history.log`.

Le playbook va également écrire sa sortie dans `/var/log/ansible-pull.log`.

Parce que ces logs indiquent rapidement et efficacement le déroulement
des opérations, le script affiche automatiquement ces deux journaux.
On peut arrêter ce défilement de logs avec la combinaison de touches `Ctrl C`.

Pour relancer le défilement de ces logs :

```shell
tail -F /var/log/ansible-pull.log /var/log/apt/history.log
```

### Vérifier les lockfiles

Lorsque l'installation démarre, des lockfiles sont écrits à
`/opt/olip-pre-install`, `/opt/olip-post-install`,
`/opt/olip-deploy`, `/opt/rebooted`.
Cela évite au script de se lancer plusieurs fois sur la même installation.

Cette commande permet de montrer ces fichiers, dates comprises :

```shell
ls -l /opt
```

Exemple de résultat :

```text
# ls -l /opt/
total 4
drwx--x--x 4 root root 4096 Jul  1 15:52 containerd
-rw-r--r-- 1 root root    0 Aug  9 15:20 olip-deploy
-rw-r--r-- 1 root root    0 Aug  9 15:21 olip-post-install
-rw-r--r-- 1 root root    0 Aug  9 15:15 olip-pre-install
-rw-r--r-- 1 root root    0 Aug  9 15:21 rebooted
```

Ici, on voit que la toute première installation d'OLIP a été effectuée
le 1er juillet ;
cependant, le dernier Deploy de Projet s'est fait le 9 août.

### Surveiller les logs de olip-api

Au bout d'un moment, toute la stack OLIP est installée,
et le CAP est redémarré.

À partir de là, les journaux système ne sont plus vraiment pertinents.
Nous avons besoin de savoir si OLIP fait son travail.

De retour sur le CAP, on redevient `root`,
puis on suit les logs du container `api` :

```shell
balena-engine logs -tf api
```

Ce que nous voulons voir, ce sont les applications
et paquets de contenus qui s'installent.

D'abord, OLIP installe les applications.
Ici, on voit l'installation du mediacenter commencer :

```text
2021-03-24T12:54:03.322420191Z 2021-03-24 12:54:03,321 - StateMachine - DEBUG - Found 1 applications to handle
2021-03-24T12:54:03.324399729Z 2021-03-24 12:54:03,323 - StateMachine - DEBUG - Launching task for application mediacenter.app from state ApplicationState.uninstalled to state ApplicationState.installed
2021-03-24T12:54:03.327103157Z 2021-03-24 12:54:03,326 - StateMachine - DEBUG - Current task <DownloadTask(Thread-4, started 3014425408)> running. Don't do anything for now
2021-03-24T12:54:03.341451266Z 2021-03-24 12:54:03,340 - DownloadTask - INFO - Pulling image offlineinternet/hugo-mediacenter-i386:latest
```

Une fois toutes les applications installées,
c'est le tour des paquets de contenus.
Ici, on voit OLIP déterminer qu'il reste 224 paquets à installer :

```shell
2021-03-24T11:03:37.272169876Z 2021-03-24 11:03:37,271 - StateMachine - DEBUG - Ping of state machine id <app.applications.state_machine.state_machine.StateMachine object at 0xb46a58e0>, current task is None
2021-03-24T11:03:37.292421484Z 2021-03-24 11:03:37,291 - StateMachine - DEBUG - Found 0 applications to handle
2021-03-24T11:03:37.711839590Z 2021-03-24 11:03:37,711 - StateMachine - DEBUG - Found 224 contents to handle
```

Bien entendu, nous espérons voir ce nombre diminuer.
On peut voir les paquets de contnus être téléchargés puis installés :

```text
2021-03-24T12:58:03.616039562Z 2021-03-24 12:58:03,613 - StateMachine - DEBUG - Ping of state machine id <app.applications.state_machine.state_machine.StateMachine object at 0xb46548b0>, current task is <InstallContentTask(Thread-6, stopped 3014425408)>
2021-03-24T12:58:03.618140300Z 2021-03-24 12:58:03,616 - StateMachine - DEBUG - Task <InstallContentTask(Thread-6, stopped 3014425408)> seems to be dead. Removing lock
2021-03-24T12:58:03.627009208Z 2021-03-24 12:58:03,626 - StateMachine - DEBUG - Found 0 applications to handle
2021-03-24T12:58:03.635908849Z 2021-03-24 12:58:03,635 - StateMachine - DEBUG - Found 13 contents to handle
2021-03-24T12:58:03.636828735Z 2021-03-24 12:58:03,635 - StateMachine - DEBUG - Launching task for content fle-niveau-3-a1-fra to state ContentState.installed
2021-03-24T12:58:03.746551883Z 2021-03-24 12:58:03,745 - urllib3.connectionpool - DEBUG - Starting new HTTP connection (1): s3.eu-central-1.wasabisys.com:80
2021-03-24T12:58:03.973329062Z 2021-03-24 12:58:03,972 - urllib3.connectionpool - DEBUG - http://s3.eu-central-1.wasabisys.com:80 "GET /olip-packages-prod/hugo/fle-niveau-3-a1-fra.zip HTTP/1.1" 200 37889152
2021-03-24T12:59:03.686465916Z no file that prove content has been added created
2021-03-24T12:59:03.686622266Z 2021-03-24 12:59:03,685 - schedule - INFO - Running job Every 1 minute do ping_state_machine() (last run: 2021-03-24 12:58:03, next run: 2021-03-24 12:59:03)
2021-03-24T12:59:03.686882273Z 2021-03-24 12:59:03,686 - StateMachine - DEBUG - Ping of state machine id <app.applications.state_machine.state_machine.StateMachine object at 0xb46548b0>, current task is <InstallContentTask(Thread-7, stopped 3014425408)>
2021-03-24T12:59:03.686958542Z 2021-03-24 12:59:03,686 - StateMachine - DEBUG - Task <InstallContentTask(Thread-7, stopped 3014425408)> seems to be dead. Removing lock
2021-03-24T12:59:03.715965660Z 2021-03-24 12:59:03,699 - StateMachine - DEBUG - Found 0 applications to handle
2021-03-24T12:59:03.716143264Z 2021-03-24 12:59:03,711 - StateMachine - DEBUG - Found 12 contents to handle
2021-03-24T12:59:03.716418807Z 2021-03-24 12:59:03,712 - StateMachine - DEBUG - Launching task for content fle-niveaux-3-et-4-fos-emploi-fra to state ContentState.installed
2021-03-24T12:59:03.826133138Z 2021-03-24 12:59:03,825 - urllib3.connectionpool - DEBUG - Starting new HTTP connection (1): s3.eu-central-1.wasabisys.com:80
2021-03-24T12:59:04.131423271Z 2021-03-24 12:59:04,130 - urllib3.connectionpool - DEBUG - http://s3.eu-central-1.wasabisys.com:80 "GET /olip-packages-prod/hugo/fle-niveaux-3-et-4-fos-emploi-fra.zip HTTP/1.1" 200 9745998
```
